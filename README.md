Undo Attenuation Benchmark
==========================

This repository contains the results of the benchmark of the paper "Anelastic
sensitivity kernels with parsimonious storage for adjoint tomography and full
waveform inversion" by D. Komatitsch, Z. Xie, E. Bozdag, E. Sales de Andrade,
D. B. Peter, Q. Liu, and J. Tromp, published in Geophys. J. Int. 2016,
doi: 10.1093/gji/ggw224. See the [LICENSE file](./LICENSE.md) for licensing
information.

Information about the exact parameters used for the simulations can be found in
the [undo_attenuation_benchmark_GJI_2016 directory][1] in the [specfem3d_globe
repository][2].

Analysis is performed with [Python 3][3] and the [Jupyter notebook][4]. Some
portions of the notebook require Fortran and the [Fortran magic extension][5].
Because the resulting kernel and simulation files are very large, they are not
included directly in this repository and you will need to use [git-annex][6] to
download those files. Additionally, due to size limits on GitLab hosting, some
simulation files which were not used for the paper are not available via the
annex either. However, you can still view the visualized results directly in
the supplied notebooks.

There are two subdirectories:
  * `3-validation`, which contains the results and visualization of section 3,
    Validation Benchmark;
  * `4-multitaper`, which contains the results and visualization of section 4,
    Importance of Full Attenuation in Kernel Calculations.

The contents of these two sections are outlined below.

[1]: https://github.com/geodynamics/specfem3d_globe/tree/devel/EXAMPLES/benchmarks/undo_attenuation_benchmark_GJI_2016
[2]: https://github.com/geodynamics/specfem3d_globe
[3]: https://www.python.org/
[4]: https://jupyter.org/
[5]: https://github.com/mgaitan/fortran_magic
[6]: https://git-annex.branchable.com/

Validation Benchmark
--------------------

As noted in the referenced paper, there are three approaches to calculating the
attenuation for a sensitivity kernel, which are contained in three
subdirectories. Each subdirectory contains the forward calculation, and the
adjoint kernel calculations for two filterings of the velocity seismogram. The
paper only includes a figure with the bandpass filter between 100 -- 200 s.
Consequently, the annex only contains the data files for this band.

There are three steps to visualizing the results from the simulations:
 1. Determining the topology and required slices of the visible portion of the
    result. Since only one hemisphere of the surface is shown, a significant
    portion of the data can be ignored. This calculation is performed by the
    [Determine Topology notebook][3.1].
 2. Extracting the surface from the `proc??????_reg1_alpha_kernel.bin` files
    and combining them into a single file, `reg_1_surface_alpha_kernel.npz`.
    This procedure is performed by the [Extract Surface notebook][3.2].
 3. Plotting the extracted surface, seismograms, and colour bars as in the
    paper. This visualization is accomplished in the [Plot Surface
    notebook][3.3].

[3.1]: https://nbviewer.jupyter.org/urls/gitlab.com/QuLogic/undo_attenuation_benchmark/raw/master/3-validation/Determine%20Topology.ipynb
[3.2]: https://nbviewer.jupyter.org/urls/gitlab.com/QuLogic/undo_attenuation_benchmark/raw/master/3-validation/Extract%20Surface.ipynb
[3.3]: https://nbviewer.jupyter.org/urls/gitlab.com/QuLogic/undo_attenuation_benchmark/raw/master/3-validation/Plot%20Surface.ipynb

Importance of Full Attenuation in Kernel Calculations
-----------------------------------------------------

Simulations were run using full attenuation and partial physical dispersion,
with kernels created for Rayleigh and Love waves, in two filtering bands,
40 -- 60 s and 40 -- 120 s. The kernel files are stored in directories named
`s40rts_<method><wave>_<band>`, where `<method>` is `fa` or `pd` for full
attenuation or partial physical dispersion, `<wave>` is `_l` or empty to denote
the Love or Rayleigh waves, and `<band>` is `T40-60s` or `T40-120s` to denote
the filter band. Only the 40 -- 60 s band is shown in the paper. Due to size
limitations on GitLab, only the surfaces extracted at 35 km and 125 km depth
are contained in the annex.

There are three steps to visualizing the results from the simulations:
 1. Determining the topology and required slices of the visible portion of the
    result. Unlike the previous section, the visible portion is the whole globe
    longitudinally and ±40° latitudinally. This calculation is performed by the
    [Determine Topology notebook][4.1].
 2. Extracting the surface from the `proc??????_reg1_bulk_beta?_kernel.bin`
    files and combining them into a single file for each depth plotted,
    `reg_1_depth*_bulk_beta?_kernel.npz`. Note that since the annex does not
    contain the full kernel files, you will not be able to run this step, and
    will have to make due with the pre-existing extracted surfaces at 35 km and
    125 km depth. Or you can run the simulation yourself, or request the full
    fileset directly from us. This procedure is performed by the [Extract Depth
    Slice notebook][4.2].
 3. Plotting the extracted surfaces, and colour bars as in the paper. Note that
    the composition of the "A" and "B" columns in the figure from the paper was
    performed in external software. This visualization is accomplished in the
    [Plot Surface notebook][4.3].

[4.1]: https://nbviewer.jupyter.org/urls/gitlab.com/QuLogic/undo_attenuation_benchmark/raw/master/4-multitaper/Determine%20Topology.ipynb
[4.2]: https://nbviewer.jupyter.org/urls/gitlab.com/QuLogic/undo_attenuation_benchmark/raw/master/4-multitaper/Extract%20Depth%20Slice.ipynb
[4.3]: https://nbviewer.jupyter.org/urls/gitlab.com/QuLogic/undo_attenuation_benchmark/raw/master/4-multitaper/Plot%20Depth%20Slice.ipynb
